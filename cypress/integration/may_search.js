
context('Search', () => {
    beforeEach(() => {

        //visit landing page
        cy.visit('https://m.aboutyou.de/')

        //revoke cookie layer
        cy.get('button#onetrust-accept-btn-handler')
          .scrollIntoView()
          .should('be.visible')
          .as('btn_accCookies')
        cy.wait(1000)
        cy.get('@btn_accCookies')
          .click()
        
    })


    it('search for nike air', () => {

        //hit magnifier icon
        cy.get('div.Header__FlexBox-i3ip1l-1.cqMOsc > a.Header__HeaderIconLink-i3ip1l-5.jklndz')
          .click()
        
        //verify that placeholder contains 'Suche'
        cy.get('input.SearchTermInput__Input-zob9fz-0')
          .type('nike air{enter}')
                
        //verify search results header
        cy.get('.SearchResultsHeader__Column-sc-8e45hm-4')
          .find('.TruncatedText-sc-1fdasdz-0')
          //show search term
          .should('have.text', 'nike air')

        //verify filter options
        cy.get('div.StreamFilterButton__FilterButton-sc-6x2u3x-0.elMNTg')
          .should('be.visible')
        
        //verify search results grid
        cy.get('.ReactVirtualized__Grid__innerScrollContainer')
          .should('be.visible')

    })
})
