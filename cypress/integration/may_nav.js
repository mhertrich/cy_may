describe('Homepage', function () {
    it('show navigation', function () {
        cy.visit('https://m.aboutyou.de/')
        cy.get('div.Header__FlexBox-i3ip1l-1.fUXerk')
          .should('be.visible')

        cy.get('div.row.navigation')
          .should('not.be.visible')
    })
  })