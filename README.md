# Cypress Test Automation - About You

This project contains a cyress test, to verify burger menu and search for term 'nike air'

## Requires
1. Install npm
```sh
$ npm install
```
2. Install node.js
Node.js 10 or 12 and above

## Installation
1. Change to project folder  
```sh
$ cd /your/project/path
```
2. Install cypress.io 
```sh
$ npm install cypress --save-dev
```
3. Clone repository
```sh
$ git clone https://mhertrich@bitbucket.org/mhertrich/cy_may.git
```

## Usage
Run cypress tests
```sh
$ node may.init.js --mobile
```
